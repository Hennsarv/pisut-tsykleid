﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            // tsükkel

            int[] arvud = { 1, 3, 2, 4, 5, 3, 6, 2, 1 };

            for (int i = 0; i < arvud.Length; i++ ) // korduse definitsioon
            {
                // siia lähevad korratavad laused
                Console.WriteLine($"{i} kohal on {arvud[i]}");
            }

            string[] õpilased = new string[20];
            for (int i = 0; i < õpilased.Length; i++)
            {
                Console.Write($"Õpilase {i} nimi: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                õpilased[i] = nimi;
            }

            foreach (string x in õpilased)
                if (x != "" && x != null) Console.WriteLine(x);

            // natuke veel tsüklitest

            bool tingimus = false; // hetkel muutuja aga miski suvaline tingimus
            // while tsükkel ja for tsükkel
            while (tingimus) {  } 
            // ja
            for (;tingimus;) {  }
            // on samaväärsed

            do { } while (tingimus);
            // ja
            for (bool b = true; b; b = tingimus) { }
            // on samaväärsed
            //



        }
    }
}
